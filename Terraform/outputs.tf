output "resource_group_name" {
  value = azurerm_resource_group.rg.name
}

# output "public_ip_address" {
#   value = azurerm_linux_virtual_machine.my_terraform_vm.public_ip_address
# }


# output "iot_edge_vm_public_ssh" {
#   value = module.iot_edge.key_data
# }

# output "iot_edge_vm_public_ssh_private" {
#   value = module.iot_edge.private_key_data
# }

# output "iot_edge_vm_public_ip" {
#   value = module.iot_edge.public_ip
# }


output "main_node_public_ssh" {
  value = module.virtual_machine.key_data
}

output "main_node_public_ssh_private" {
  value = module.virtual_machine.private_key_data
}

output "main_node_public_ip" {
  value = module.virtual_machine.public_ip
}

# output "main_node_script_output" {
#   value = module.virtual_machine.script_output
# }

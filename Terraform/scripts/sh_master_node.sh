#!/bin/bash


sudo apt install snapd > /dev/null

# kubectl
sudo snap install kubectl --classic > /dev/null
# helm
sudo snap install helm --classic > /dev/null

# microk8s
sudo snap install microk8s --classic > /dev/null
echo "

------------------------------------------------Install Complete---------------------------------------------
------------------------------------------------Install Complete---------------------------------------------
------------------------------------------------Install Complete---------------------------------------------
------------------------------------------------Install Complete---------------------------------------------
------------------------------------------------Install Complete---------------------------------------------
"






# microk8s setting
sudo microk8s enable dashboard  > /dev/null
sudo microk8s disable ha-cluster --force > /dev/null

sudo microk8s config > ~/.config > /dev/null

sudo microk8s kubectl -n kube-system patch service kubernetes-dashboard -p '{"spec": {"type": "NodePort"}}' > /dev/null




sudo microk8s kubectl create token default

# kudeedge

sudo wget https://github.com/kubeedge/kubeedge/releases/download/v1.9.2/keadm-v1.9.2-linux-amd64.tar.gz > /dev/null

sudo mv keadm-v1.9.2-linux-amd64.tar.gz /usr/local/bin/keadm > /dev/null

sudo chmod +x /usr/local/bin/keadm > /dev/null


# get public ip

sudo apt install curl jq

public_ip=$(curl -s https://api64.ipify.org?format=json | jq -r '.ip') 

# 打印获取到的公共IP地址
# echo "Public IP Address: $public_ip"

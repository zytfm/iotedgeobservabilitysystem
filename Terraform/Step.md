## 1 Init
```  
    terraform init -upgrade 
    terraform import "azurerm_resource_group.rg" "/subscriptions/973f4e86-ba38-4314-93a0-262cb2a183eb/resourceGroups/IoT_DevOps_ZY"
    terraform plan -out main.tfplan
    terraform apply main.tfplan

```
## 2 destroy

```
    terraform plan -destroy -out main.destroy.tfplan
    terraform apply main.destroy.tfplan
```

## 3 state

```
    terraform show 
    terraform refresh

```


/subscriptions/973f4e86-ba38-4314-93a0-262cb2a183eb/resourceGroups/IoT_DevOps_ZY

PS H:\Study Files\TFM\iotedgeobservabilitysystem\Terraform> 

https://github.com/EliiseS/terraform-iot-edge-vm

https://github.com/Azure/terraform-azurerm-virtual-machine/tree/1.1.0



# in this case
ssh -i ~/.ssh/iot/iotedge.pem user_vm_name@40.114.200.53

ssh -i ~/.ssh/iot/vm.pem azureadmin@13.81.217.33
# resource "random_pet" "rg_name" {
#   prefix = var.resource_group_name_prefix
# }

resource "azurerm_resource_group" "rg" {
  location = var.resource_group_location
  # name     = random_pet.rg_name.id
  name = "IoT_DevOps_ZY"
}


module "virtual_machine" {
  source                  = "./modules/virtual-machine"
  resource_group_name     = azurerm_resource_group.rg.name
  resource_group_location = var.resource_group_location
  resource_group_id = azurerm_resource_group.rg.id
  username                = var.username

}


# module "iot_hub" {
#   source                  = "./modules/iot-hub"
#   resource_group_name     = azurerm_resource_group.rg.name
#   resource_group_location = var.resource_group_location
#   resource_prefix         = var.resource_group_name_prefix
# }

# module "iot_edge" {
#   source                        = "./modules/iot-edge"
#   resource_prefix               = var.resource_group_name_prefix
#   iot_hub_name                  = module.iot_hub.iot_hub_name
#   resource_group_name           = azurerm_resource_group.rg.name
#   location                      = var.resource_group_location
#   vm_user_name                  = "user_vm_name"
#   # edge_device_connection_string = module.iot_hub.edge_device_connection_string
#   resource_group_id             = azurerm_resource_group.rg.id
# }


variable "resource_group_name" {
  type = string
}

variable "resource_group_location" {
  type = string
}

variable "resource_group_id" {
  type = string
}

variable "username" {
  type        = string
  description = "The username for the local account that will be created on the new VM."
  default     = "azureadmin"
}
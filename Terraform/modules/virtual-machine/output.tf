output "key_data" {
  value = jsondecode(azapi_resource_action.ssh_public_key_gen.output).publicKey
}

output "private_key_data" {
  value = jsondecode(azapi_resource_action.ssh_public_key_gen.output).privateKey
}

output "public_ip" {
  value = azurerm_public_ip.my_terraform_public_ip.ip_address
}

# output "script_output" {
#   value = azurerm_virtual_machine_extension.my_terraform_vm_extension.settings
# }
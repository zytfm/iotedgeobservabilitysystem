

resource "azurerm_iothub" "iot_hub" {
  name                          = "${var.resource_prefix}-iot-hub"
  resource_group_name           = var.resource_group_name
  location                      = var.resource_group_location
  public_network_access_enabled = true

  sku {
    name     = "S1"
    capacity = "1"
  }

  fallback_route {
    source         = "DeviceMessages"
    endpoint_names = ["events"]
    enabled        = true
  }

  tags = {
    purpose = "testing"
  }
}



# resource "null_resource" "run_shell_script" {
#   provisioner "local-exec" {
#     command = <<EOT
#       az iot hub device-identity create --device-id "$${var.resource_prefix}-edge-device" --edge-enabled --hub-name "${azurerm_iothub.iot_hub.name}" --resource-group "${azurerm_iothub.iot_hub.resource_group_name}" --output none
#     EOT

#     # environment = {
#     #   IOT_HUB_NAME         = azurerm_iothub.iot_hub.name
#     #   RESOURCE_GROUP       = azurerm_iothub.iot_hub.resource_group_name
#     #   IOT_EDGE_DEVICE_NAME = "${var.resource_prefix}-edge-device"


#     # }



#   }
# }
# resource "azurerm_resource_deployment_script_azure_power_shell" "run_shell_script" {
#   name                = "run_shell_script"
#   resource_group_name = azurerm_iothub.iot_hub.resource_group_name
#   location            = "West Europe"
#   version             = "8.3"
#   retention_interval  = "P1D"
#   cleanup_preference  = "OnSuccess"


#   script_content = <<EOF
#           az iot hub device-identity create --device-id "${var.resource_prefix}-edge-device" --edge-enabled --hub-name "${azurerm_iothub.iot_hub.name}" --resource-group "${azurerm_iothub.iot_hub.resource_group_name}" --output none
#   EOF

  
# }





# resource "shell_script" "register_iot_edge_device" {
#   lifecycle_commands {
#     # create = "$SCRIPT create"
#     # read   = "$SCRIPT read"
#     # delete = "$SCRIPT delete"
#     create = file("${path.module}/scripts/create.sh")
#     read   = file("${path.module}/scripts/read.sh")
#     delete = file("${path.module}/scripts/delete.sh")
#   }

#     //machine will be used (/bin/sh for linux/mac and cmd for windows)
#   # interpreter = ["cmd", "-c"]


#   environment = {
#     # IOT_HUB_NAME         = azurerm_iothub.iot_hub.name
#     # RESOURCE_GROUP       = azurerm_iothub.iot_hub.resource_group_name
#     # IOT_EDGE_DEVICE_NAME = "${var.resource_prefix}-edge-device"
#     NAME        = "HELLO-WORLD"
#     DESCRIPTION = "description"
#     # SCRIPT               = "./register_iot_edge_device.sh"
#   }
# }

#https://registry.terraform.io/providers/scottwinkler/shell/latest/docs/resources/shell_script_resource

# output "public_ssh" {
#   # value = "ssh -i ../.ssh/id_rsa ${local.vm_user_name}@${azurerm_public_ip.iot_edge.fqdn}"
#   value = tls_private_key.vm_ssh.private_key_pem
# }


output "key_data" {
  value = jsondecode(azapi_resource_action.ssh_public_key_gen.output).publicKey
}

output "private_key_data" {
  value = jsondecode(azapi_resource_action.ssh_public_key_gen.output).privateKey
}

output "public_ip" {
  value = azurerm_public_ip.iot_edge.ip_address
}
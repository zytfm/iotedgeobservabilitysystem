# IoTEdgeObservabilitySystem



## Introduction

Here you can find all codes, configuration files and resources used in the IoT edge Kubernetes System and Monitoring & Observability System

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/zytfm/iotedgeobservabilitysystem.git
git branch -M main
git push -uf origin main
```

## First Step

# Terraform and Set up Resource

- [Terraform] (https://gitlab.com/zytfm/iotedgeobservabilitysystem/-/tree/main/Terraform?ref_type=heads) Files and Documents
  - Set up Resources group, IoT Hub, VM for master node, one VM to simulate edge node
  - [OneDevice] (https://learn.microsoft.com/zh-cn/azure/iot-edge/how-to-provision-single-device-linux-symmetric?view=iotedge-1.4&tabs=azure-cli%2Cubuntu) register one only device (Optional)
  - [dps] (https://learn.microsoft.com/zh-cn/azure/iot-edge/how-to-provision-devices-at-scale-linux-symmetric?view=iotedge-1.4&tabs=individual-enrollment%2Cubuntu) to set up group of edge device in azure (Optional)
  - 
- [Azure] Set up Edge (Optional)
- [VM] Connect to VM using SSH generated
  - ssh -i ~/.ssh/id_rsa.pem username@vm_ip

# Kubernetes
- copy the script sh_master_node.sh into your master node and run it ./sh_master_node.sh <MASTERNODE_IP>
  - you will get a token, save it
- cp the script sh_edge_node.sh into your edge node and run it ./sh_edge_node.sh <token> <IP>
- 


## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/zytfm/iotedgeobservabilitysystem/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Set auto-merge](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing (SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

## Name
IoT edge Kubernetes and Monitoring & Observability System

## Description
The goal of this project is to implement a Kubernetes-based IoT Edge system and control and maximize the effectiveness of these complex systems in terms of self-healing and self-tuning, 

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
Releases in the future:



## Authors and acknowledgment
Zhengyu Ye

## License


## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
